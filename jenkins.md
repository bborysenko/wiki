<!-- TITLE: Jenkins -->
<!-- SUBTITLE: A quick summary of Jenkins -->

# Jenkins

http://localhost/jenkins/plugins#плагины

## Плагины

- [Locale](https://wiki.jenkins.io/display/JENKINS/Locale+Plugin) - позволяет явно указать необходимый язык интерфейса без привязки к браузеру.
- `[gitlab-plugin](https://plugins.jenkins.io/gitlab-plugin)` - запуск задач по тригеру при пуше кода или при открытии Merge Request в Gitlab
- [Github Plugin](https://wiki.jenkins.io/display/JENKINS/Github+Plugin)
- [Jenkins Mailer Plugin](https://www.notion.so/bborysenko/Jenkins-936d6b3ab1ef440dafcd7f3ef9b98c2b)